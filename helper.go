package wt

func dashIfEmpty(s string) string {
	if s == "" {
		return "-"
	}
	return s
}
