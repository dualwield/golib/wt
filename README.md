# Web Token Lib


### Token Format
The token consists of three segments `VERSION.PAYLOAD.SIGNATURE`. Both `PAYLOAD` and
`SIGNATURE` are base64 encoded, `VERSION` is not.




### Signature Instructions
The signature is based upon the base64 encoded representation of the payload. Do not
sign the raw payload.

1. Convert the payload to a base64 representation, this will be the 2nd segment of token.
2. With HMAC and the SHA512 algorithm, create the signature using the base64 encoded payload
and as the signing key, use the secret prepended by the version represented in segment one.