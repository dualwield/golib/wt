package wt

import "testing"

func TestClaimInAndOut(t *testing.T) {
	key := []byte("xyz123456789")
	payload := []byte("[some notation holding an object]")
	tokenizedClaim := New(payload, key, V1)
	var claim Claim

	if err := claim.Read(tokenizedClaim); err != nil {
		t.Error(err)
	}
	if !ValidateSignature(claim, key) {
		t.Error("failed to validate signature")
	}
}

func TestClaimFails(t *testing.T) {
	key := []byte("xyz123456789")
	payload := []byte("[some notation holding an object]")
	tokenizedClaim := New(payload, key, V1)
	var claim Claim

	if err := claim.Read(tokenizedClaim); err != nil {
		t.Error(err)
	}
	if ValidateSignature(claim, []byte("an incorrect key")) {
		t.Error("signature should have not passed test")
	}
}
