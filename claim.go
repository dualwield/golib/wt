package wt

import (
	"encoding/base64"
	"errors"
	"fmt"
	"strings"
)

const V1 = "wt1"

const format = `%s.%s.%s`

func New(payload, key []byte, version string) string {
	b64Pay := base64.URLEncoding.EncodeToString(payload)
	signature := base64.URLEncoding.EncodeToString(Sign([]byte(b64Pay), append([]byte(version), key...)))

	return fmt.Sprintf(format, version, b64Pay, signature)
}

func ValidateSignature(claim Claim, key []byte) bool {
	b64Pay := base64.URLEncoding.EncodeToString(claim.Payload)
	expected := base64.URLEncoding.EncodeToString(Sign([]byte(b64Pay), append([]byte(claim.Version), key...)))
	return expected == claim.Signature
}

type Claim struct {
	Payload   []byte
	Signature string
	Version   string
}

func (c *Claim) Tokenize() string {
	return fmt.Sprintf(format, c.Version, base64.URLEncoding.EncodeToString(c.Payload), dashIfEmpty(c.Signature))
}

func (c *Claim) Read(token string) error {
	if strings.Count(token, ".") != 2 {
		return errors.New(`parse failure: WT Claims must have three segments, with a delimiter of "."`)
	}

	version, encPayload, encSignature := func() (string, string, string) {
		split := strings.Split(token, ".")
		return split[0], split[1], split[2]
	}()

	c.Version = version
	c.Signature = encSignature

	err := func() error {
		decPay, err := base64.URLEncoding.DecodeString(encPayload)
		if err != nil {
			return err
		}
		c.Payload = decPay
		return nil
	}()
	if err != nil {
		return err
	}
	return nil
}
