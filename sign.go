package wt

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
)

func Sign(payload, secret []byte) []byte {
	h := hmac.New(sha512.New, secret)
	h.Write(payload)
	return []byte(hex.EncodeToString(h.Sum(nil)))
}
