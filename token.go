package wt

type Token string

func EmptyUnsigned() (c Claim) {
	c.Version = V1
	c.Payload = []byte("{}")
	return
}
